﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Product.Models;
using Product.AppDB;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;

namespace Product.Controllers
{
    public class LoginController : Controller
    {
        private readonly ApplicationDbContext dbContext;
        private readonly SignInManager<IdentityUser> signInManager;
        public LoginController(ApplicationDbContext dbContext, SignInManager<IdentityUser> signInManager)
        {
            this.dbContext = dbContext;
            this.signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Index (Login model, bool returnUrl = true)
        //{
        //    ViewData["ReturnUrl"] = returnUrl;
        //    if (ModelState.IsValid)
        //    {
        //        Microsoft.AspNetCore.Identity.SignInResult result = await signInManager.PasswordSignInAsync(model.StaffID, model.Password, isPersistent: true, lockoutOnFailure: false);
        //        if (result.Succeeded)
        //        {
        //            return RedirectToAction(nameof(HProductsController.Index), "HProducts");
        //        } else
        //        {
        //            ModelState.AddModelError(string.Empty, "Invalid login attemp.");
        //            return View(model);
        //        }
        //    }
        //    return View(model);
        //}
        public async Task<ActionResult> Login(Login model)
        {
            if (ModelState.IsValid)
            {
                var userdetails = await dbContext.UserDetails.SingleOrDefaultAsync(m => m.StaffID == model.StaffID && m.Password == model.Password);
                if (userdetails == null)
                {
                    ModelState.AddModelError("Password", "Invalid login attempt.");
                    return View("Index");
                }
                //HttpContext.Session.Set("userId");
            }
            else
            {
                return View("Index");
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> Register(Register model)
        {
            if (ModelState.IsValid)
            {
                UserDetail user = new UserDetail
                {
                    Name = model.Name,
                    StaffID = model.StaffID,
                    Password = model.Password,
                };
                dbContext.Add(user);
                await dbContext.SaveChangesAsync();
            }
            else
            {
                return View("Registration");
            }
            return RedirectToAction("Index", "Login");
        }

        public IActionResult Registration()
        {
            ViewData["Message"] = "Registration Paage";
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}
