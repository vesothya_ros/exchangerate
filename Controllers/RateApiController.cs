﻿using Microsoft.AspNetCore.Mvc;
using Product.AppDB;
using Product.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Product.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RateApiController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        public RateApiController(ApplicationDbContext context, UserManager<IdentityUser> userManager) 
        {  
            _context = context;
            _userManager = userManager;
        }

        [HttpGet]
        [Route("GetData")]
        public async Task<ActionResult<IEnumerable<HProduct>>> GetData()
        {
            if(_context.Products == null)
            {
                return NotFound();
            }
            return await _context.Products.ToListAsync();
        }

        public async Task<IActionResult> Register([FromBody] Login model)
        {
            var user = await _userManager.FindByIdAsync(model.StaffID);
            if (user != null)
                return StatusCode(500, "Already bro!");

            IdentityUser staffid = new()
            {
                UserName = model.StaffID
            };

            var result = await _userManager.CreateAsync(staffid, model.Password);
            if(!result.Succeeded)
                return StatusCode(500, "Again bro.");

            return StatusCode(200, "Good to go bro. Thanks!");
        }
    }
}
