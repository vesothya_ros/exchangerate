﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Product.AppDB;
using Product.Migrations;
using Product.Models;

namespace Product.Controllers
{
    public class HProductsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HProductsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HProducts
        public async Task<IActionResult> Index()
        {
              return _context.Products != null ? 
                          View(await _context.Products.ToListAsync()) :
                          Problem("Entity set 'ApplicationDbContext.Products'  is null.");
        }

        // GET: HProducts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var hProduct = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hProduct == null)
            {
                return NotFound();
            }

            return View(hProduct);
        }

        // GET: HProducts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HProducts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProductName,ProductRate,CreatedDate,CreatedBy")] HProduct product)
        {
            //if (ModelState.IsValid)
            //{
            //    await _context.AddAsync(product);
            //    await _context.SaveChangesAsync();
            //    return RedirectToAction(nameof(Index));
            //}
            //return View(product);
            if(ModelState.IsValid)
            {
                var productRate = new HProduct
                {
                    ProductName = product.ProductName,
                    ProductRate = product.ProductRate,
                    CreatedDate = DateTime.Now,
                    CreatedBy = "HTB",
                };
                await _context.AddAsync(productRate);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: HProducts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var hProduct = await _context.Products.FindAsync(id);
            if (hProduct == null)
            {
                return NotFound();
            }
            return View(hProduct);
        }

        // POST: HProducts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProductName,ProductRate,UpdatedBy")] HProduct hProduct)
        {
            if (id != hProduct.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var product = new HProduct
                    {
                        ProductName = hProduct.ProductName,
                        ProductRate = hProduct.ProductRate,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = "HTB",
                    };
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HProductExists(hProduct.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hProduct);
        }

        // GET: HProducts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var hProduct = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hProduct == null)
            {
                return NotFound();
            }

            return View(hProduct);
        }

        // POST: HProducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Products == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Products'  is null.");
            }
            var hProduct = await _context.Products.FindAsync(id);
            if (hProduct != null)
            {
                _context.Products.Remove(hProduct);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HProductExists(int id)
        {
          return (_context.Products?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
