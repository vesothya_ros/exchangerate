﻿namespace Product.Models
{
    public class HProduct
    {
        public int Id { get; set; }
        public string? ProductName { get; set; }
        public string? ProductRate { get; set;}
        public DateTime CreatedDate { get; set;} = DateTime.Now;
        public string? CreatedBy { get; set; }
        public string? UpdatedBy { get; set; }
        public string? AuthorizedBy { get; set; } 
    }
}
