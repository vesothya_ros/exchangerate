﻿namespace Product.Models
{
    public class Register
    {
        public string StaffID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
