﻿namespace Product.Models
{
    public class Login
    {
        public string StaffID { get; set; }
        public string Password { get; set; }
    }
}
