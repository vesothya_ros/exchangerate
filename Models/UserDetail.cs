﻿namespace Product.Models
{
    public class UserDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string StaffID { get; set; }
        public string Password { get; set; }
    }
}
